import axios from "axios";
import router from "./router";

const api = axios.create({
  baseURL: "http://localhost:8000/api",
  // validatedStatus: true,
  headers: {
    "Content-Type": "application/json",
  },
});

// Add a request interceptor
api.interceptors.request.use(
  function (config) {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers["Authorization"] = token;
    }
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
api.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.status === 401) {
      if (confirm("Sesi anda telah habis, silahkan login kembali")) {
        localStorage.removeItem("token");
        router.push({ name: "login-page" });
      }
    }
    return Promise.reject(error);
  }
);

export default api;
