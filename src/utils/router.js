import { createRouter, createWebHistory } from "vue-router";
import Guest from "../layouts/Guest.vue";
import Auth from "../layouts/Auth.vue";

const routes = [
  {
    path: "/",
    redirect: { name: "login-page" },
  },
  {
    path: "/",
    component: Guest,
    children: [
      {
        path: "login",
        component: () => import("../pages/Login.vue"),
        name: "login-page",
      },
      {
        path: "register",
        component: () => import("../pages/Register.vue"),
      },
      {
        path: "/:pathMatch(.*)*",
        name: "NotFound",
        component: () => import("../pages/NotFound.vue"),
      },
    ],
  },
  {
    path: "/app",
    component: Auth,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "dashboard",
        component: () => import("../pages/app/Dashboard.vue"),
      },
      {
        path: "products",
        component: () => import("../pages/app/product/ListProduct.vue"),
      },
      {
        path: "products/create",
        component: () => import("../pages/app/product/CreateProduct.vue"),
      },
      {
        path: "products/edit/:productId",
        component: () => import("../pages/app/product/EditProduct.vue"),
        name: "product-edit",
      },
      {
        path: "customers",
        component: () => import("../pages/app/customer/ListCustomer.vue"),
      },
      {
        path: "customers/create",
        component: () => import("../pages/app/customer/CreateCustomer.vue"),
      },
      {
        path: "customers/edit/:customerId",
        component: () => import("../pages/app/customer/EditCustomer.vue"),
        name: "customer-edit",
      },
      {
        path: "transactions",
        component: () => import("../pages/app/transaction/ListTransaction.vue"),
      },
      {
        path: "transactions/create",
        component: () =>
          import("../pages/app/transaction/CreateTransaction.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("token") != null;
  if (to.name === "login-page" && token) {
    next({ name: "login-page" });
  } else if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!token) {
      next({
        path: "/login",
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
